package main

import (
	"log/slog"
	api "tuesday/pkg"
)

func main() {
	slog.SetLogLoggerLevel(slog.LevelDebug)

	config := api.Config{
		DB:   "tuesday.db",
		Port: 3000,
	}
	api.Start(config)
}
