package utils

type Null[T any] struct {
	Value T
	valid bool
}

func (n Null[_]) IsValid() bool {
	return n.valid
}

func Nullable[T any](value T, valid bool) Null[T] {
	c := Null[T]{Value: value, valid: valid}
	return c
}
