package utils

import (
	"fmt"
	"net/url"
	"strings"
	"unicode/utf8"

	"github.com/microcosm-cc/bluemonday"
)

func PrintReadTime(minutes int64) string {
	hours := minutes / 60
	remainingMinutes := minutes % 60

	if hours > 0 {
		return fmt.Sprintf("%d hour, %d min read", hours, remainingMinutes)
	} else {
		return fmt.Sprintf("%d min read", minutes)
	}
}

func SanitizeHTML(str string) string {
	p := bluemonday.UGCPolicy()
	return p.Sanitize(str)
}

func SanitizeAndStripHTML(str string) string {
	p := bluemonday.StrictPolicy()
	return p.Sanitize(str)
}

func TruncateChars(str string, maxLength int) string {
	if len(str) <= maxLength {
		return str
	}

	maxRunes := maxLength

	var truncated []rune
	for len(str) > 0 {
		r, size := utf8.DecodeRuneInString(str)
		truncated = append(truncated, r)
		str = str[size:]

		if len(truncated) == maxRunes {
			break
		}
	}

	return string(truncated)
}

func URLStringHasHTTPScheme(urlStr string) bool {
	u, err := url.Parse(urlStr)
	if err != nil {
		return false
	}

	scheme := strings.ToLower(u.Scheme)
	return scheme == "http" || scheme == "https"
}
