package utils

import (
	"fmt"
	"time"
)

func ParseDBTimeString(timedate string) (time.Time, error) {
	t, err := time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", timedate)
	if err != nil {
		return time.Now(), err
	}

	return t, nil
}

func TimeSince(timedate string) (string, error) {
	t, err := ParseDBTimeString(timedate)
	if err != nil {
		return "", err
	}

	now := time.Now()
	duration := now.Sub(t)

	// Calculate the duration in months, weeks, days, and hours
	months := int(duration.Hours() / (24 * 30.4375)) // Assuming 30.4375 days per month
	weeks := int(duration.Hours() / (24 * 7))
	days := int(duration.Hours()/24) % 7
	hours := int(duration.Hours()) % 24

	// Round to the nearest month if weeks >= 4
	if weeks >= 4 {
		months = int(float64(weeks)/4.0 + 0.5) // Round to the nearest integer
		weeks = 0
	}

	// Format the duration based on the largest non-zero unit
	if months > 0 {
		return fmt.Sprintf("%d months ago", months), nil
	} else if weeks > 0 {
		return fmt.Sprintf("%d weeks ago", weeks), nil
	} else if days > 0 {
		return fmt.Sprintf("%d days ago", days), nil
	} else {
		return fmt.Sprintf("%d hours ago", hours), nil
	}
}

func AbbrFormatTimeString(timedate string) (string, error) {
	t, err := ParseDBTimeString(timedate)
	if err != nil {
		return "", err
	}

	return t.Format("Jan 1, 2006"), nil
}

func FormatTimeString(timedate string) (string, error) {
	t, err := ParseDBTimeString(timedate)
	if err != nil {
		return "", err
	}

	month := t.Month().String()[:3] // Get the first 3 characters of the month name
	day := t.Day()

	// Format the day with an appropriate suffix
	// If the day is between 11 and 13, use "th" as the suffix
	daySuffix := "th"
	switch day % 10 {
	case 1:
		daySuffix = "st"
	case 2:
		daySuffix = "nd"
	case 3:
		daySuffix = "rd"
	}

	if day%100/10 == 1 {
		daySuffix = "th"
	}

	return fmt.Sprintf("%s %d%s", month, day, daySuffix), nil
}
