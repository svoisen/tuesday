package utils

import (
	"testing"
	"tuesday/internal/test"
)

func TestSanitizeAndStripHTML(t *testing.T) {
	str := "<h1>This is a test</h1>"
	expected := "This is a test"
	actual := SanitizeAndStripHTML(str)
	test.Equals(t, expected, actual)
}

func TestTruncateChars(t *testing.T) {
	str := "This is a very long string of characters."
	expected := "This is a"
	actual := TruncateChars(str, 9)
	test.Equals(t, expected, actual)
}
