package transport

import (
	"context"
	"log/slog"
	"net/http"
	"tuesday/internal/components"
	"tuesday/internal/services"

	"github.com/a-h/templ"
)

type searchHandler struct {
	context       context.Context
	searchService services.SearchService
}

func ActivateSearchRoutes(router *http.ServeMux, searchService services.SearchService) {
	createSearchHandler(router, searchService)
}

func createSearchHandler(router *http.ServeMux, searchService services.SearchService) {
	handler := searchHandler{
		searchService: searchService,
		context:       context.Background(),
	}

	router.HandleFunc("GET /{$}", handler.home)
}

func (h *searchHandler) home(w http.ResponseWriter, r *http.Request) {
	slog.Info("[SearchHandler] GET /")

	query := r.URL.Query().Get("q")
	if query != "" {
		h.runQuery(query, w, r)
	} else {
		h.showInbox(w, r)
	}
}

func (h *searchHandler) runQuery(query string, w http.ResponseWriter, r *http.Request) {
	slog.Debug("[SearchHandler] running query", "query", query)

	items, err := h.searchService.Search(h.context, query)
	if err != nil {
		slog.Error("[SearchHandler] error running query", "error", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var component templ.Component

	if ShouldRenderFullPage(r) {
		pageParams := components.PageParams{
			SidebarContent:  components.Navigation(components.NoNavItem),
			MainContent:     components.InboxList(items),
			ShowSearchInput: true,
		}
		component = components.Page(pageParams)
	} else {
		component = components.InboxList(items)
	}

	RenderComponent(component, h.context, w)
}

func (h *searchHandler) showInbox(w http.ResponseWriter, r *http.Request) {
	items, err := h.searchService.GetRecent(h.context)
	if err != nil {
		slog.Error("[SearchHandler] error getting recents", "error", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var component templ.Component

	if ShouldRenderFullPage(r) {
		pageParams := components.PageParams{
			SidebarContent:  components.Navigation(components.InboxNavItem),
			MainContent:     components.InboxList(items),
			ShowSearchInput: true,
		}
		component = components.Page(pageParams)
	} else {
		component = components.InboxList(items)
	}

	RenderComponent(component, h.context, w)
}
