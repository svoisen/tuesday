package transport

import (
	"net/http"
	"tuesday/internal/services"
)

func CreateRouter(feedService services.FeedService, searchService services.SearchService) *http.ServeMux {
	router := http.NewServeMux()

	// Handle static assets
	router.Handle("GET /assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))

	ActivateFeedRoutes(router, feedService)
	ActivateSearchRoutes(router, searchService)
	ActivateReadRoutes(router, searchService)

	return router
}
