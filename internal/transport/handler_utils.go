package transport

import (
	"context"
	"log/slog"
	"net/http"
	"strconv"

	"github.com/a-h/templ"
)

func RenderComponent(component templ.Component, context context.Context, w http.ResponseWriter) {
	err := component.Render(context, w)
	if err != nil {
		slog.Error("error rendering component", "error", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// isHxmlRequest returns true if the given request was issued by HXML.
// HXML provides a HX-Request header that is set to true, which we use
// to check if the request is from a HXML-based application.
func IsHtmxRequest(r *http.Request) bool {
	header := r.Header
	return header.Get("HX-Request") == "true"
}

func IsHtmxHistoryRestoreRequest(r *http.Request) bool {
	header := r.Header
	return header.Get("HX-History-Restore-Request") == "true"
}

func ShouldRenderFullPage(r *http.Request) bool {
	return IsHtmxHistoryRestoreRequest(r) || !IsHtmxRequest(r)
}

func GetIDFromPath(r *http.Request) (int64, error) {
	id_str := r.PathValue("id")
	id, err := strconv.Atoi(id_str)
	if err != nil {
		slog.Error("invalid path id", "id", id_str, "error", err.Error())
		return -1, err
	}

	return int64(id), nil
}
