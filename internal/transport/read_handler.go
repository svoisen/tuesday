package transport

import (
	"context"
	"log/slog"
	"net/http"
	"tuesday/internal/components"
	"tuesday/internal/services"

	"github.com/a-h/templ"
)

type readHandler struct {
	context       context.Context
	searchService services.SearchService
}

func ActivateReadRoutes(router *http.ServeMux, searchService services.SearchService) {
	createReadHandler(router, searchService)
}

func createReadHandler(router *http.ServeMux, searchService services.SearchService) {
	handler := readHandler{
		searchService: searchService,
		context:       context.Background(),
	}

	router.HandleFunc("GET /read/{id}", handler.readItem)
}

func (h *readHandler) readItem(w http.ResponseWriter, r *http.Request) {
	slog.Info("[ReadHandler] GET /read/{id}")

	id, err := GetIDFromPath(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	item, err := h.searchService.Get(h.context, int64(id))
	if err != nil {
		slog.Error("[ReadHandler] error getting item", "id", id, "error", err.Error())
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	var component templ.Component

	if ShouldRenderFullPage(r) {
		pageParams := components.PageParams{
			SidebarContent:  components.Navigation(components.NoNavItem),
			MainContent:     components.ReadView(item.Value),
			ShowSearchInput: false,
		}
		component = components.Page(pageParams)
	} else {
		component = components.ReadView(item.Value)
	}

	RenderComponent(component, h.context, w)
}
