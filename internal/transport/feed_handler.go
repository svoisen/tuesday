package transport

import (
	"context"
	"log/slog"
	"net/http"
	"tuesday/internal/components"
	"tuesday/internal/services"
	. "tuesday/internal/utils"

	"github.com/a-h/templ"
)

type feedHandler struct {
	context     context.Context
	feedService services.FeedService
}

func ActivateFeedRoutes(router *http.ServeMux, feedService services.FeedService) {
	createFeedHandler(router, feedService)
}

func createFeedHandler(router *http.ServeMux, feedService services.FeedService) {
	handler := feedHandler{
		feedService: feedService,
		context:     context.Background(),
	}

	router.HandleFunc("GET /feeds", handler.listFeeds)
	router.HandleFunc("POST /feeds", handler.addFeed)
	router.HandleFunc("GET /feeds/new", handler.newFeed)
	router.HandleFunc("DELETE /feeds/{id}", handler.deleteFeed)
}

func (h *feedHandler) listFeeds(w http.ResponseWriter, r *http.Request) {
	slog.Info("[FeedHandler] GET /feeds")
	feeds, err := h.feedService.GetAll(h.context)
	if err != nil {
		slog.Error("[FeedHandler] error getting feeds", "error", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var component templ.Component

	if ShouldRenderFullPage(r) {
		pageParams := components.PageParams{
			SidebarContent:  components.Navigation(components.FeedsNavItem),
			MainContent:     components.FeedsList(feeds),
			ShowSearchInput: false,
		}
		component = components.Page(pageParams)
	} else {
		component = components.FeedsList(feeds)
	}

	RenderComponent(component, h.context, w)
}

func (h *feedHandler) addFeed(w http.ResponseWriter, r *http.Request) {
	feedUrl := r.FormValue("feed-url")
	slog.Info("[FeedHandler] POST /feeds", "URL", feedUrl)

	if !URLStringHasHTTPScheme(feedUrl) {
		feedUrl = "https://" + feedUrl
	}

	_, err := h.feedService.Add(h.context, feedUrl)
	if err != nil {
		slog.Error("[FeedHandler] error adding feed", "error", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	feeds, err := h.feedService.GetAll(h.context)
	if err != nil {
		slog.Error("[FeedHandler] error getting feeds", "error", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	component := components.FeedsList(feeds)
	RenderComponent(component, h.context, w)
}

func (h *feedHandler) newFeed(w http.ResponseWriter, r *http.Request) {
	slog.Debug("[FeedHandler] GET /feeds/new")

	w.Header().Add("HX-Trigger-After-Settle", "modal-inserted")
	component := components.ModalDialog(components.AddFeedDialogForm())
	RenderComponent(component, h.context, w)
}

func (h *feedHandler) deleteFeed(w http.ResponseWriter, r *http.Request) {
	slog.Debug("[FeedHandler] DELETE /feeds/{id}")

	id, err := GetIDFromPath(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	err = h.feedService.Delete(h.context, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
}
