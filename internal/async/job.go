package async

type Job struct {
	Name string
	Run  func() error
}
