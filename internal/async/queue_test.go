package async_test

import (
	"fmt"
	"testing"
	"time"
	"tuesday/internal/async"
	"tuesday/internal/test"
)

func TestNewQueue(t *testing.T) {
	q := async.NewQueue("Test")
	test.Equals(t, "Test", q.Name)
	test.Assert(t, q.Jobs != nil, "jobs should not be nil")
}

func TestAddRunJobs(t *testing.T) {
	count := 0
	q := async.NewQueue("Test")
	jobs := make([]async.Job, 0)
	createJob := func(num int) {
		jobs = append(jobs, async.Job{
			Run: func() error {
				fmt.Printf("Job %d\n", num)
				count++
				time.Sleep(1 * time.Millisecond)
				return nil
			},
		})
	}

	for i := 0; i < 5; i++ {
		createJob(i)
	}
	q.Enqueue(jobs)
	worker := async.NewWorker(q)
	done := make(chan int)
	go worker.DoWork(done)
	<-done
	test.Equals(t, 5, count)
}
