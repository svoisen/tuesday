package async

import "log/slog"

type Worker struct {
	queue *Queue
}

func NewWorker(queue *Queue) *Worker {
	return &Worker{
		queue: queue,
	}
}

func (w *Worker) DoWork(done chan int) bool {
	for {
		select {
		case <-w.queue.Ctx.Done():
			slog.Info("Work done in queue", "name", w.queue.Name, "error", w.queue.Ctx.Err())
			done <- 1
			return true

		case job := <-w.queue.Jobs:
			err := job.Run()
			if err != nil {
				continue
			}
		}
	}
}
