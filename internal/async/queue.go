package async

import (
	"context"
	"log/slog"
	"sync"
)

type Queue struct {
	Name   string
	Jobs   chan Job
	Ctx    context.Context
	Cancel context.CancelFunc
}

func NewQueue(name string) *Queue {
	ctx, cancel := context.WithCancel(context.Background())

	return &Queue{
		Name:   name,
		Jobs:   make(chan Job),
		Ctx:    ctx,
		Cancel: cancel,
	}
}

func (q *Queue) Enqueue(jobs []Job) {
	var wg sync.WaitGroup
	wg.Add(len(jobs))

	for _, job := range jobs {
		go func(job Job) {
			q.addJob(job)
			wg.Done()
		}(job)
	}

	go func() {
		wg.Wait()
		q.Cancel()
	}()
}

func (q *Queue) addJob(job Job) {
	slog.Info("Adding job to queue", "name", q.Name)
	q.Jobs <- job
}
