package components

import "tuesday/internal/models"

templ FeedsList(feeds []models.Feed) {
    <div class="px-4 lg:py-8 sm:py-6 sm:px-6 lg:px-8 h-full">
        @ListHeader(
            ListHeaderParams{
                title: "Feeds", 
                description: "Add, remove or edit RSS feeds.",
                addButton: len(feeds) > 0,
                addButtonLabel: "Add feed",
                addButtonPath: "/feeds/new",
        })
        if (len(feeds) == 0) {
            @EmptyFeedsStarter()
        } else {
        <div class="mt-8 flow-root">
            <div class="-mx-4 -my-2 sm:-mx-6 lg:-mx-8">
                <div class="inline-block min-w-full py-2 align-middle">
                    <table class="min-w-full border-separate border-spacing-0">
                        <thead>
                            <tr>
                                <th scope="col" class="sticky top-0 z-10 border-b border-gray-300 bg-white bg-opacity-75 py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter sm:pl-6 lg:pl-8">Name</th>
                                <th scope="col" class="sticky top-0 z-10 hidden border-b border-gray-300 bg-white bg-opacity-75 px-3 py-3.5 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter sm:table-cell">Description</th>
                                <th scope="col" class="sticky top-0 z-10 hidden border-b border-gray-300 bg-white bg-opacity-75 px-3 py-3.5 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter lg:table-cell">Last updated</th>
                                <th scope="col" class="sticky top-0 z-10 border-b border-gray-300 bg-white bg-opacity-75 px-3 py-3.5 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter">Articles</th>
                                <th scope="col" class="sticky top-0 z-10 border-b border-gray-300 bg-white bg-opacity-75 py-3.5 pl-3 pr-4 backdrop-blur backdrop-filter sm:pr-6 lg:pr-8">
                                    <span class="sr-only">Edit</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            for _, feed := range feeds {
                                @FeedsListItem(feed)
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        }
    </div>
}