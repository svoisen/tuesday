package services

import (
	"context"
	"errors"
	"log/slog"
	"net/url"
	"tuesday/internal/models"
	"tuesday/internal/stores"
	. "tuesday/internal/utils"

	"github.com/mmcdole/gofeed"
)

type FeedService interface {
	Get(ctx context.Context, id int64) (Null[models.Feed], error)
	GetAll(ctx context.Context) ([]models.Feed, error)
	Add(ctx context.Context, feedUrl string) (Null[models.Feed], error)
	Create(ctx context.Context, feedUrl string) (Null[models.Feed], error)
	Delete(ctx context.Context, id int64) error
}

type service struct {
	feedStore stores.FeedStore
	itemStore stores.ItemStore
}

func NewFeedService(feedStore stores.FeedStore, itemStore stores.ItemStore) FeedService {
	return &service{feedStore: feedStore, itemStore: itemStore}
}

func (s *service) Get(ctx context.Context, id int64) (Null[models.Feed], error) {
	return s.feedStore.Get(id)
}

// Creates a new feed without persisting it to storage.
func (s *service) Create(ctx context.Context, feedUrl string) (Null[models.Feed], error) {
	parsedUrl, err := url.ParseRequestURI(feedUrl)
	if err != nil {
		slog.Error("[FeedService.Create] invalid feed URL", "url", feedUrl, "error", err.Error())
		return Nullable(models.Feed{}, false), err
	}

	// TODO: Use a timeout
	// Fetch the feed so we can get the title
	parser := gofeed.NewParser()
	parsedFeed, err := parser.ParseURL(parsedUrl.String())
	if err != nil {
		slog.Error("[FeedService.Create] no valid feed at URL", "url", feedUrl, "error", err.Error())
		return Nullable(models.Feed{}, false), err
	}

	feed, err := models.CreateFeed(parsedFeed, feedUrl)
	return Nullable(feed, true), err
}

func (s *service) Add(ctx context.Context, feedUrl string) (Null[models.Feed], error) {
	feed, err := s.Create(ctx, feedUrl)
	if err != nil || !feed.IsValid() {
		slog.Error("[FeedService.Add] could not create Feed", "error", err.Error())
		return Nullable(models.Feed{}, false), err
	}

	feedID, err := s.feedStore.Add(feed.Value)
	if err != nil {
		return Nullable(models.Feed{}, false), err
	}

	if feedID == 0 {
		slog.Error("[FeedService.Add] unexpected feedID 0")
		return Nullable(models.Feed{}, false), errors.New("unexpected feedID 0")
	}

	feed.Value.FeedID = feedID
	for i := range feed.Value.Items {
		feed.Value.Items[i].FeedID = feedID
	}

	s.itemStore.AddAll(feed.Value.Items)
	return feed, nil
}

func (s *service) GetAll(ctx context.Context) ([]models.Feed, error) {
	return s.feedStore.GetAll()
}

func (s *service) Delete(ctx context.Context, id int64) error {
	return s.feedStore.Delete(id)
}
