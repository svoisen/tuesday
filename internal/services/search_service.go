package services

import (
	"context"
	"tuesday/internal/models"
	"tuesday/internal/stores"
	. "tuesday/internal/utils"
)

type SearchService interface {
	GetRecent(ctx context.Context) ([]models.Item, error)
	Get(ctx context.Context, id int64) (Null[models.Item], error)
	Search(ctx context.Context, search string) ([]models.Item, error)
}

type searchService struct {
	itemStore stores.ItemStore
}

func NewSearchService(itemStore stores.ItemStore) SearchService {
	return &searchService{itemStore: itemStore}
}

func (s *searchService) GetRecent(ctx context.Context) ([]models.Item, error) {
	return s.itemStore.GetRecent()
}

func (s *searchService) Get(ctx context.Context, id int64) (Null[models.Item], error) {
	return s.itemStore.Get(id)
}

func (s *searchService) Search(ctx context.Context, query string) ([]models.Item, error) {
	return s.itemStore.Search(query)
}
