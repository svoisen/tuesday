package stores

import (
	"log/slog"
	"tuesday/internal/models"

	. "tuesday/internal/utils"

	"github.com/jmoiron/sqlx"
)

type FeedStore interface {
	Get(id int64) (Null[models.Feed], error)
	GetAll() ([]models.Feed, error)
	GetByUrl(feedUrl string) (Null[models.Feed], error)
	Add(feed models.Feed) (int64, error)
	Delete(id int64) error
}

type feedStore struct {
	db *sqlx.DB
}

func NewFeedStore(db *sqlx.DB) FeedStore {
	return &feedStore{db}
}

// Get gets a Feed from the database.
func (s *feedStore) Get(id int64) (Null[models.Feed], error) {
	slog.Debug("[FeedStore] get feed", "id", id)
	feed := models.Feed{}
	err := s.db.Get(&feed, "SELECT * from feeds WHERE (feed_id=?)", id)
	if err != nil {
		return Nullable(models.Feed{}, false), err
	}

	return Nullable(feed, true), nil
}

func (s *feedStore) GetAll() ([]models.Feed, error) {
	slog.Debug("[FeedStore] getting all feeds")
	feeds := make([]models.Feed, 0)
	err := s.db.Select(&feeds, "SELECT * FROM feeds")
	if err != nil {
		return nil, err
	}

	return feeds, nil
}

func (s *feedStore) GetByUrl(feedUrl string) (Null[models.Feed], error) {
	slog.Debug("[FeedStore] get feed", "url", feedUrl)
	feed := models.Feed{}
	err := s.db.Get(&feed, "SELECT * from feeds WHERE (url=?)", feedUrl)
	if err != nil {
		return Nullable(models.Feed{}, false), err
	}

	return Nullable(feed, true), nil
}

// Add adds a feed to the database.
// Add returns the ID of the feed.
func (s *feedStore) Add(feed models.Feed) (int64, error) {
	slog.Debug("[FeedStore] adding feed", "title", feed.Name, "url", feed.URL)
	result, err := s.db.NamedExec("INSERT INTO feeds (url, name, description, updated) VALUES (:url, :name, :description, :updated)", feed)
	if err != nil {
		return -1, err
	}

	newID, _ := result.LastInsertId()
	return newID, nil
}

func (s *feedStore) Delete(id int64) error {
	slog.Debug("[FeedStore] deleting feed", "id", id)
	tx, err := s.db.Beginx()
	if err != nil {
		return err
	}

	_, err = s.db.Exec("DELETE FROM items WHERE (feed_id=?)", id)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = s.db.Exec("DELETE FROM feeds WHERE (feed_id=?)", id)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}
