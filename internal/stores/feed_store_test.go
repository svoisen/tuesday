package stores_test

import (
	"log"
	"os"
	"testing"
	"time"
	"tuesday/internal/models"
	"tuesday/internal/stores"
	"tuesday/internal/test"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var db *sqlx.DB

func dropFeedsTable(t *testing.T) {
	_, err := db.Exec("DROP TABLE feeds")
	if err != nil {
		t.Fatalf("Failed to drop table %s: %v", "feeds", err)
	}
}

func createStoreAndFeed() (stores.FeedStore, models.Feed) {
	db.MustExec(models.FeedSchema)
	store := stores.NewFeedStore(db)
	feed := models.Feed{
		Name:    "Test Feed",
		URL:     "http://example.com/feed",
		Updated: time.Now().String(),
	}
	store.Add(feed)

	return store, feed
}

func TestMain(m *testing.M) {
	file, err := os.CreateTemp("", "test.*.db")
	if err != nil {
		log.Fatal(err)
	}
	file.Close()

	db, err = sqlx.Connect("sqlite3", file.Name())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	code := m.Run()

	os.Remove(file.Name())
	os.Exit(code)
}

func TestGetEmptyDatabase(t *testing.T) {
	t.Cleanup(func() {
		dropFeedsTable(t)
	})

	db.MustExec(models.FeedSchema)
	store := stores.NewFeedStore(db)
	maybeFeed, err := store.Get(1)
	test.NotOk(t, err)
	test.Assert(t, maybeFeed.IsValid() == false, "feed should be invalid")
}

func TestGetFeed(t *testing.T) {
	t.Cleanup(func() {
		dropFeedsTable(t)
	})

	store, _ := createStoreAndFeed()
	maybeFeed, err := store.Get(1)
	test.Ok(t, err)
	test.Assert(t, maybeFeed.IsValid() == true, "feed should be valid")
}

func TestGetFeedWithExistingUrl(t *testing.T) {
	t.Cleanup(func() {
		dropFeedsTable(t)
	})
	db.MustExec(models.FeedSchema)

	store, feed := createStoreAndFeed()
	maybeFeed, err := store.GetByUrl(feed.URL)
	test.Ok(t, err)
	test.Assert(t, maybeFeed.IsValid(), "feed should be valid")
	test.Equals(t, feed.URL, maybeFeed.Value.URL)
}

func TestGetFeedWithNonExistingUrl(t *testing.T) {
	t.Cleanup(func() {
		dropFeedsTable(t)
	})
	db.MustExec(models.FeedSchema)

	store, _ := createStoreAndFeed()
	maybeFeed, err := store.GetByUrl("https://foo.com/feed")
	test.NotOk(t, err)
	test.Assert(t, maybeFeed.IsValid() == false, "feed should be invalid")
}

func TestAddReturnsFeedID(t *testing.T) {
	t.Cleanup(func() {
		dropFeedsTable(t)
	})

	db.MustExec(models.FeedSchema)
	store := stores.NewFeedStore(db)
	feed := models.Feed{
		Name:    "Test Feed",
		URL:     "http://example.com/feed",
		Updated: time.Now().String(),
	}
	id, err := store.Add(feed)
	test.Ok(t, err)
	test.Assert(t, id == 1, "feed_id should be 1")
}

func TestAddDoesNotAddDuplicate(t *testing.T) {
	t.Cleanup(func() {
		dropFeedsTable(t)
	})

	db.MustExec(models.FeedSchema)
	store := stores.NewFeedStore(db)
	feed := models.Feed{
		Name:    "Test Feed",
		URL:     "http://example.com/feed",
		Updated: time.Now().String(),
	}
	_, err := store.Add(feed)
	test.Ok(t, err)

	_, err = store.Add(feed)
	test.NotOk(t, err)
}
