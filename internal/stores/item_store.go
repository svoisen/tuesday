package stores

import (
	"errors"
	"log/slog"
	"strconv"
	"tuesday/internal/models"

	. "tuesday/internal/utils"

	"github.com/jmoiron/sqlx"
)

type ItemStore interface {
	Add(item models.Item) (int64, error)
	AddAll(items []models.Item) error
	Get(id int64) (Null[models.Item], error)
	GetRecent() ([]models.Item, error)
	Search(query string) ([]models.Item, error)
}

type itemStore struct {
	db *sqlx.DB
}

func NewItemStore(db *sqlx.DB) ItemStore {
	return &itemStore{db}
}

func (s *itemStore) Get(id int64) (Null[models.Item], error) {
	slog.Debug("[ItemStore] getting item", "item_id", id)
	item := models.Item{}
	err := s.db.Get(&item, "SELECT i.*, f.name AS feed_name, f.description AS feed_description FROM items i JOIN feeds f ON i.feed_id = f.feed_id WHERE (i.item_id=?)", id)
	if err != nil {
		return Nullable(models.Item{}, false), err
	}

	return Nullable(item, true), nil
}

func (s *itemStore) GetRecent() ([]models.Item, error) {
	slog.Debug("[ItemStore] getting recent items")
	items := make([]models.Item, 0)
	err := s.db.Select(&items, "SELECT i.*, f.name AS feed_name, f.description AS feed_description FROM items i JOIN feeds f ON i.feed_id = f.feed_id ORDER BY published DESC LIMIT 10")
	if err != nil {
		slog.Error("[ItemStore] error getting recent items", "error", err.Error())
	}

	return items, err
}

func (s *itemStore) AddAll(items []models.Item) error {
	slog.Debug("[ItemStore] adding items")

	tx, err := s.db.Beginx()
	if err != nil {
		slog.Error("[ItemStore] error starting transaction", "error", err.Error())
		return err
	}

	for _, item := range items {
		_, err = s.Add(item)
		if err != nil {
			slog.Error("[ItemStore] could not persist item", "error", err.Error())
			tx.Rollback()
			return err
		}
	}

	return tx.Commit()
}

func (s *itemStore) Add(item models.Item) (int64, error) {
	slog.Debug("[ItemStore] adding item", "title", item.Title)

	if !item.IsValid() {
		slog.Error("[ItemStore] cannot persist invalid item")
		return -1, errors.New("cannot persist invalid item")
	}

	result, err := s.db.NamedExec(`
		INSERT INTO items (title, link, published, updated, excerpt, content, read_time, author_name, feed_id) 
		VALUES (:title, :link, :published, :updated, :excerpt, :content, :read_time, :author_name, :feed_id)
		`,
		item)
	if err != nil {
		slog.Error("[ItemStore] cannot insert item", "error", err.Error())
		return -1, err
	}

	newId, _ := result.LastInsertId()
	return newId, nil
}

func (s *itemStore) Update(item models.Item) error {
	if !item.IsValid() || item.ItemID <= 1 {
		return errors.New("cannot update invalid item")
	}

	slog.Debug("[ItemStore] updating item", "id", strconv.Itoa(int(item.ItemID)))
	_, err := s.db.NamedExec(`
		UPDATE items SET title = :title
		WHERE item_id = :item_id`,
		item)

	if err != nil {
		slog.Error("[ItemStore] cannot update item", "error", err.Error())
		return err
	}

	return nil
}

func (s *itemStore) Search(query string) ([]models.Item, error) {
	items := make([]models.Item, 0)
	err := s.db.Select(&items, "SELECT i.* FROM item_content ic JOIN items i ON i.item_id = ic.rowid WHERE ic.content MATCH '"+query+"'")
	if err != nil {
		slog.Error("[ItemStore] error running search", "query", query, "error", err.Error())
	}

	return items, err
}
