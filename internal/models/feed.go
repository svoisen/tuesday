package models

import (
	"errors"
	"log/slog"
	"strings"

	"github.com/mmcdole/gofeed"
)

var FeedSchema = `
CREATE TABLE IF NOT EXISTS feeds (
	feed_id INTEGER PRIMARY KEY AUTOINCREMENT,
	url TEXT NOT NULL UNIQUE,
	name TEXT NOT NULL,
	description TEXT,
	updated TEXT NOT NULL
);`

type Feed struct {
	FeedID      int64  `db:"feed_id"`
	Name        string `db:"name"`
	URL         string `db:"url"`
	Description string `db:"description"`
	Updated     string `db:"updated"`
	Items       []Item
}

func CreateFeed(feed *gofeed.Feed, url string) (Feed, error) {
	if feed == nil {
		// TODO: We should probably return an error
		return Feed{}, errors.New("cannot create feed from nil pointer")
	}

	updated := feed.Updated

	if feed.UpdatedParsed != nil {
		updated = feed.UpdatedParsed.String()
	}

	model := Feed{
		URL:         url,
		Name:        strings.TrimSpace(feed.Title),
		Description: feed.Description,
		Updated:     updated,
		Items:       make([]Item, 0),
	}

	slog.Debug("Creating feed", "updated", updated)

	for _, item := range feed.Items {
		model.Items = append(model.Items, CreateItem(item))
	}

	return model, nil
}
