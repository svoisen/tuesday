package models

import (
	"log/slog"
	"strings"
	"tuesday/internal/utils"

	"github.com/mmcdole/gofeed"
)

var ItemSchema = `
CREATE TABLE IF NOT EXISTS items (
	item_id INTEGER PRIMARY KEY AUTOINCREMENT,
	title TEXT NOT NULL,
	link TEXT NOT NULL,
	published TEXT NOT NULL,
	updated TEXT NOT NULL,
	excerpt TEXT,
	content TEXT,
	read_time INTEGER NOT NULL,
	author_name TEXT,
	feed_id INTEGER NOT NULL,
	FOREIGN KEY (feed_id)
		REFERENCES feeds (feed_id)
);`

var ItemContentSchema = `
CREATE VIRTUAL TABLE IF NOT EXISTS item_content USING fts5(
	title, 
	content, 
	content='items', 
	content_rowid='item_id'
);`

var ItemContentAfterInsertTrigger = `
CREATE TRIGGER item_ai AFTER INSERT ON items
    BEGIN
        INSERT INTO item_content (rowid, title, content)
        VALUES (new.item_id, new.title, new.content);
    END;`

type Item struct {
	ItemID          int64  `db:"item_id"`
	Title           string `db:"title"`
	Link            string `db:"link"`
	Published       string `db:"published"`
	Updated         string `db:"updated"`
	Content         string `db:"content"`
	Excerpt         string `db:"excerpt"`
	AuthorName      string `db:"author_name"`
	ReadTime        int64  `db:"read_time"`
	FeedID          int64  `db:"feed_id"`
	FeedName        string `db:"feed_name"`
	FeedDescription string `db:"feed_description"`
}

// https://swiftread.com/blog/average-reading-speed-analysis
const avgWPM = 238

func computeReadTime(text string) int64 {
	words := strings.Fields(text)
	wordCount := len(words)

	minutes := int64(float64(wordCount) / float64(avgWPM))
	return minutes
}

func createExcerpt(content string, maxLength int) string {
	return utils.TruncateChars(utils.SanitizeAndStripHTML(content), maxLength)
}

func CreateItem(item *gofeed.Item) Item {
	if item == nil {
		// TODO: We should probably return an error
		return Item{}
	}

	slog.Debug("[Item] creating item", "title", item.Title)

	published := item.Published
	if item.PublishedParsed != nil {
		published = item.PublishedParsed.String()
	}

	updated := item.Updated
	if item.UpdatedParsed != nil {
		updated = item.UpdatedParsed.String()
	}

	authors := make([]string, 0)
	for _, author := range item.Authors {
		authors = append(authors, author.Name)
	}

	return Item{
		Title:      item.Title,
		Link:       item.Link,
		Published:  published,
		Updated:    updated,
		AuthorName: strings.Join(authors, ", "),
		Content:    utils.SanitizeHTML(item.Content),
		Excerpt:    createExcerpt(item.Content, 250),
		ReadTime:   computeReadTime(item.Content),
	}
}

func (item Item) IsValid() bool {
	return len(strings.TrimSpace(item.Title)) > 0 && len(strings.TrimSpace(item.Link)) > 0 && item.FeedID != 0
}
