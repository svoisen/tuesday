package models_test

import (
	"strings"
	"testing"
	"tuesday/internal/models"
	"tuesday/internal/test"

	"github.com/mmcdole/gofeed"
)

func TestCreateFeedFromEmpty(t *testing.T) {

}

func TestCreateFeedTrimsTitle(t *testing.T) {
	mockUrl := "https://example.com/feed"
	mockFeedData := gofeed.Feed{
		Title: "  A Title  ",
	}

	actual, err := models.CreateFeed(&mockFeedData, mockUrl)
	expected := strings.TrimSpace(mockFeedData.Title)

	test.Ok(t, err)
	test.Equals(t, expected, actual.Name)
}
