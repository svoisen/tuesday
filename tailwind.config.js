/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./internal/components/**/*.templ"],
  theme: {
    extend: {
      zIndex: {
        '100': '100',
      }
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
