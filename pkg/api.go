package api

import (
	"log"
	"log/slog"
	"net/http"
	"strconv"
	"tuesday/internal/models"
	"tuesday/internal/services"
	"tuesday/internal/stores"
	"tuesday/internal/transport"

	_ "github.com/mattn/go-sqlite3"

	"github.com/jmoiron/sqlx"
)

type Config struct {
	DB   string
	Port int
}

func Start(config Config) {
	db, err := sqlx.Connect("sqlite3", config.DB)
	if err != nil {
		log.Fatalf("Failed to open database %s: %v", config.DB, err)
	}

	defer db.Close()

	// Create database schema
	db.MustExec(models.FeedSchema)
	db.MustExec(models.ItemSchema)
	db.MustExec(models.ItemContentSchema)
	db.MustExec(models.ItemContentAfterInsertTrigger)

	// Create stores
	itemStore := stores.NewItemStore(db)
	feedStore := stores.NewFeedStore(db)

	// Create services
	feedService := services.NewFeedService(feedStore, itemStore)
	searchService := services.NewSearchService(itemStore)

	// Create router
	router := transport.CreateRouter(feedService, searchService)

	port := ":" + strconv.Itoa(config.Port)
	slog.Info("Server listening on " + port)
	log.Fatal(http.ListenAndServe(port, router))
}
