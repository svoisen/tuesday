export class ModalDialog extends HTMLElement {
    constructor() {
        super();

        this.handleCancelClick = this.handleCancelClick.bind(this);
        this.handleActionClick = this.handleActionClick.bind(this);
    }

    connectedCallback() {
        this.backdrop = this.querySelector('[data-id="modal-backdrop"]');
        this.cancelButton = this.querySelector('[data-id="cancel-btn"]');
        this.actionButton = this.querySelector('[data-id="action-btn"]');
        this.dialog = this.querySelector('[data-id="modal-contents"]');

        if (this.cancelButton) {
            this.cancelButton.addEventListener("click", this.handleCancelClick);
        }

        if (this.actionButton) {
            this.actionButton.addEventListener("click", this.handleActionClick);
        }
    }

    disconnectedCallback() {
        if (this.cancelButton) {
            this.cancelButton.removeEventListener("click", this.handleCancelClick);
        }

        if (this.actionButton) {
            this.actionButton.removeEventListener("click", this.handleActionClick);
        }
    }

    handleActionClick() {
        this.hideAndDestroy();
    }

    handleCancelClick() {
        this.hideAndDestroy();
    }

    show() {
        this.backdrop.classList.remove("opacity-0");
        this.backdrop.classList.add("opacity-100");

        this.dialog.classList.remove("opacity-0", "translate-y-4", "sm:translate-y-0", "sm:scale-95");
        this.dialog.classList.add("opacity-100", "translate-y-0", "sm:scale-100");
    }

    hideAndDestroy() {
        this.backdrop.addEventListener("transitionend", () => this.destroy());

        this.backdrop.classList.remove("ease-out", "opacity-100");
        this.backdrop.classList.add("ease-in", "opacity-0");

        this.dialog.classList.remove("ease-out", "opacity-100", "translate-y-0", "sm:scale-100");
        this.dialog.classList.add("ease-in", "opacity-0", "translate-y-4", "sm:translate-y-0", "sm:scale-95");
    }

    destroy() {
        this.remove();
    }
}
