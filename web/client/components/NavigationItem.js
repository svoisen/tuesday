export class NavigationItem extends HTMLElement {
    constructor() {
        super();

        this.handleHistoryChange = this.handleHistoryChange.bind(this);
    }

    connectedCallback() {
        this.link = this.querySelector('[data-id="link"]');

        document.addEventListener("htmx:pushedIntoHistory", this.handleHistoryChange);
        document.addEventListener("htmx:replacedInHistory", this.handleHistoryChange);
        
        this.updateSelectedStyles();
    }

    disconnectedCallback() {
        document.removeEventListener("htmx:pushedIntoHistory", this.handleHistoryChange);
        document.removeEventListener("htmx:replacedInHistory", this.handleHistoryChange);
    }

    handleHistoryChange() {
        this.updateSelectedStyles();
    }

    updateSelectedStyles() {
        const selected = this.getAttribute("data-path") === window.location.pathname;

        if (selected) {
            this.link.classList.remove("text-gray-400", "hover:bg-gray-800")
            this.link.classList.add("text-white", "bg-gray-800")
        } else {
            this.link.classList.remove("text-white", "bg-gray-800")
            this.link.classList.add("text-gray-400", "hover:bg-gray-800")
        }
    }
}