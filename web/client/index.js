import { ModalDialog } from "./components/ModalDialog.js"
import { NavigationItem } from "./components/NavigationItem.js";

function initApp() {
    // This event is emitted via server-side HTMX headers
    document.addEventListener("modal-inserted", () => {
        const dialog = document.querySelector("tu-modal-dialog");
        if (dialog) {
            dialog.show();
        }
    });
}

customElements.define("tu-modal-dialog", ModalDialog);
customElements.define("tu-navigation-item", NavigationItem);

initApp();